package com.epam.edu.online.model;

public interface Command {
    void execute(String string);
}
