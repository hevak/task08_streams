package com.epam.edu.online.model;

@FunctionalInterface
public interface MyFunctional {
    Integer execute(Integer first, Integer second, Integer third);
}
