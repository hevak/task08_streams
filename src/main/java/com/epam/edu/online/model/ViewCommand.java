package com.epam.edu.online.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewCommand {
    private final static String DEFAULT_STRING = "User entered command: ";
    private static Scanner scanner = new Scanner(System.in);
    private Map<Commands, Command> commandMap = fillCommandMap();

    public void executeExample() {
        String commandStr;
        do {
            System.out.print("Enter command: ");
            commandStr = scanner.next();
            try {
                executeCommandByArgument(commandStr).execute(commandStr);
            } catch (NullPointerException e) {
                System.out.println("`" + commandStr + "` is not recognized as an internal " +
                        "or external command.\n Enter `help` to display all commands");
            }
        } while (!commandStr.equalsIgnoreCase(Commands.Q.name()));
    }

    private Command executeCommandByArgument(String commandStr) {
        for (Map.Entry<Commands, Command> command : commandMap.entrySet()) {
            if (commandStr.equalsIgnoreCase(command.getKey().name())) {
                return command.getValue();
            }
        }
        return string -> System.out.println("good by");
    }


    private Map<Commands, Command> fillCommandMap() {
        Map<Commands, Command> commandMap = new HashMap<>();
        commandMap.put(Commands.ANONYMOUS, this::anonymous);
        commandMap.put(Commands.LAMBDA, this::lambda);
        commandMap.put(Commands.REFERENCE, this::reference);
        commandMap.put(Commands.HELP, this::help);
        return commandMap;
    }

    private void help(String s) {
        commandMap.keySet().forEach(System.out::println);
    }

    private void reference(String arg) {
        System.out.print(DEFAULT_STRING);
        Command command = System.out::println;
        command.execute(arg);
    }

    private void lambda(String arg) {
        Command command = string -> {
            System.out.println(DEFAULT_STRING + string);
        };
        command.execute(arg);
    }
    private void anonymous(String arg) {
        Command command = new Command() {
            @Override
            public void execute(String arg) {
                System.out.println(DEFAULT_STRING + arg);
            }
        };
        command.execute(arg);
    }
}
