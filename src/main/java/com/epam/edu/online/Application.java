package com.epam.edu.online;

import com.epam.edu.online.model.MyFunctional;
import com.epam.edu.online.model.ViewCommand;

public class Application {

    public static void main(String[] args) {
        task1();
        new ViewCommand().executeExample();
    }


    private static void task1() {
        // create lambda
        MyFunctional max = (first, second, third) -> Math.max(Math.max(first, second), third);
        MyFunctional average = (first, second, third) -> (first + second + third) / 3;
        // test result
        System.out.println(max.execute(5, 8, 4));
        System.out.println(average.execute(5, 8, 4));
    }
}
